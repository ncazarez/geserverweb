create table if not exists aplicacion_rasp
(
    id                 int unsigned auto_increment
        primary key,
    numero_serie_banda varchar(50)  not null,
    numero_serie       varchar(50)  not null,
    descripcion        varchar(255) not null,
    estado_activo      varchar(1)   not null,
    created_at         timestamp    null,
    updated_at         timestamp    null
)
    collate = utf8mb4_unicode_ci;

create table if not exists aseguradoras
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(40)          not null,
    descripcion   varchar(255)         null,
    estado_activo tinyint(1) default 1 not null,
    created_at    timestamp            null,
    updated_at    timestamp            null
)
    collate = utf8mb4_unicode_ci;

create table if not exists categoria_reactivos
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(100)         not null,
    descripcion   varchar(255)         not null,
    estado_activo tinyint(1) default 1 not null,
    created_at    timestamp            null,
    updated_at    timestamp            null
)
    collate = utf8mb4_unicode_ci;

create table if not exists estados_territorios
(
    id            int unsigned auto_increment
        primary key,
    created_at    timestamp            null,
    updated_at    timestamp            null,
    nombre        varchar(50)          not null,
    siglas        varchar(20)          not null,
    estado_activo tinyint(1) default 1 not null
)
    collate = utf8mb4_unicode_ci;

create table if not exists marca_vehiculos
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(50) not null,
    estado_activo tinyint(1)  not null,
    created_at    timestamp   null,
    updated_at    timestamp   null
)
    collate = utf8mb4_unicode_ci;

create table if not exists migrations
(
    id        int unsigned auto_increment
        primary key,
    migration varchar(255) not null,
    batch     int          not null
)
    collate = utf8mb4_unicode_ci;

create table if not exists modelos
(
    id                int unsigned auto_increment
        primary key,
    nombre            varchar(20)          not null,
    estado_activo     tinyint(1) default 1 not null,
    marca_vehiculo_id int unsigned         not null,
    created_at        timestamp            null,
    updated_at        timestamp            null,
    constraint modelos_marca_vehiculo_id_foreign
        foreign key (marca_vehiculo_id) references marca_vehiculos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists modulos
(
    id            int unsigned auto_increment
        primary key,
    created_at    timestamp            null,
    updated_at    timestamp            null,
    nombre        varchar(50)          not null,
    descripcion   varchar(255)         not null,
    estado_activo tinyint(1) default 1 not null
)
    collate = utf8mb4_unicode_ci;

create table if not exists checklist_instrumentos
(
    id                int unsigned auto_increment
        primary key,
    folio_instrumento varchar(15)          not null,
    nombre            varchar(100)         not null,
    descripcion       varchar(255)         not null,
    estado_activo     tinyint(1) default 1 not null,
    modulo_id         int unsigned         not null,
    created_at        timestamp            null,
    updated_at        timestamp            null,
    constraint checklist_instrumentos_modulo_id_foreign
        foreign key (modulo_id) references modulos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists password_resets
(
    email      varchar(255) not null,
    token      varchar(255) not null,
    created_at timestamp    null
)
    collate = utf8mb4_unicode_ci;

create index password_resets_email_index
    on password_resets (email);

create table if not exists permisos
(
    id            int unsigned auto_increment
        primary key,
    created_at    timestamp            null,
    updated_at    timestamp            null,
    nombre        varchar(10)          not null,
    estado_activo tinyint(1) default 1 not null,
    modulo_id     int unsigned         not null,
    constraint permisos_modulo_id_foreign
        foreign key (modulo_id) references modulos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists placa_vehiculos
(
    id                   int unsigned auto_increment
        primary key,
    created_at           timestamp    null,
    updated_at           timestamp    null,
    estado_activo        tinyint(1)   not null,
    num_placa            varchar(20)  not null,
    tarjeta_circulacion  varchar(20)  not null,
    imagen_placa         varchar(255) not null,
    vigencia_desde       date         not null,
    vigencia_hasta       date         not null,
    estado_territorio_id int unsigned not null,
    constraint placa_vehiculos_estado_territorio_id_foreign
        foreign key (estado_territorio_id) references estados_territorios (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists regions
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(50)      not null,
    estado_activo double default 1 not null,
    created_at    timestamp        null,
    updated_at    timestamp        null
)
    collate = utf8mb4_unicode_ci;

create table if not exists bases
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(20)          not null,
    estado_activo tinyint(1) default 1 not null,
    region_id     int unsigned         not null,
    created_at    timestamp            null,
    updated_at    timestamp            null,
    constraint bases_region_id_foreign
        foreign key (region_id) references regions (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists roles
(
    id            int unsigned auto_increment
        primary key,
    created_at    timestamp   null,
    updated_at    timestamp   null,
    nombre        varchar(50) not null,
    estado_activo tinyint(1)  not null
)
    collate = utf8mb4_unicode_ci;

create table if not exists permisos_negados
(
    id         int unsigned auto_increment
        primary key,
    created_at timestamp    null,
    updated_at timestamp    null,
    rol_id     int unsigned not null,
    permiso_id int unsigned not null,
    constraint permisos_negados_permiso_id_foreign
        foreign key (permiso_id) references permisos (id),
    constraint permisos_negados_rol_id_foreign
        foreign key (rol_id) references roles (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists sub_categoria_reactivos
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(100)         not null,
    descripcion   varchar(255)         not null,
    estado_activo tinyint(1) default 1 not null,
    categoria_id  int unsigned         not null,
    created_at    timestamp            null,
    updated_at    timestamp            null,
    constraint sub_categoria_reactivos_categoria_id_foreign
        foreign key (categoria_id) references categoria_reactivos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists reactivos
(
    id                        int unsigned auto_increment
        primary key,
    texto_respuesta           varchar(100)         not null,
    tipo_respuesta            varchar(100)         not null,
    comentario                varchar(255)         not null,
    texto_reactivo            varchar(100)         not null,
    critico                   tinyint(1) default 1 not null,
    estado_activo             tinyint(1) default 1 not null,
    sub_categoria_reactivo_id int unsigned         not null,
    created_at                timestamp            null,
    updated_at                timestamp            null,
    constraint reactivos_sub_categoria_reactivo_id_foreign
        foreign key (sub_categoria_reactivo_id) references sub_categoria_reactivos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists tipo_identificaciones
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(255) not null,
    estado_activo tinyint(1)   not null,
    created_at    timestamp    null,
    updated_at    timestamp    null
)
    collate = utf8mb4_unicode_ci;

create table if not exists tipo_unidad_vehiculos
(
    id          int unsigned auto_increment
        primary key,
    tipo_unidad varchar(50) not null,
    created_at  timestamp   null,
    updated_at  timestamp   null
)
    collate = utf8mb4_unicode_ci;

create table if not exists unidad_medidas
(
    id            int unsigned auto_increment
        primary key,
    nombre        varchar(50) not null,
    estado_activo tinyint(1)  not null,
    created_at    timestamp   null,
    updated_at    timestamp   null
)
    collate = utf8mb4_unicode_ci;

create table if not exists users
(
    id                 int unsigned auto_increment
        primary key,
    correo_electronico varchar(50)                    not null,
    password           varchar(100)                   not null,
    nombre             varchar(80)                    not null,
    apaterno           varchar(80)                    not null,
    amaterno           varchar(80)                    not null,
    genero             varchar(255) default 'hombre'  not null,
    celular            varchar(100)                   not null,
    rol_id             int unsigned                   not null,
    puesto             varchar(50)                    not null,
    tipo_empleado      varchar(255) default 'interno' not null,
    esta_activo        tinyint(1)                     not null,
    remember_token     varchar(100)                   null,
    created_at         timestamp                      null,
    updated_at         timestamp                      null,
    constraint users_correo_electronico_unique
        unique (correo_electronico),
    constraint users_rol_id_foreign
        foreign key (rol_id) references roles (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists domicilios
(
    id                   int unsigned auto_increment
        primary key,
    calle                varchar(50)  not null,
    entre                varchar(100) not null,
    numero               varchar(8)   not null,
    codigo_postal        varchar(5)   not null,
    colonia              varchar(50)  not null,
    ciudad               varchar(50)  not null,
    municipio            varchar(50)  not null,
    estado_activo        tinyint(1)   not null,
    estado_territorio_id int unsigned not null,
    usuarios_id          int unsigned not null,
    unidad_negocio_id    int unsigned not null,
    created_at           timestamp    null,
    updated_at           timestamp    null,
    constraint domicilios_estado_territorio_id_foreign
        foreign key (estado_territorio_id) references estados_territorios (id),
    constraint domicilios_usuarios_id_foreign
        foreign key (usuarios_id) references users (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists user_identificaciones
(
    id                     int unsigned auto_increment
        primary key,
    folio                  varchar(255) not null,
    vigencia_desde         datetime     not null,
    vigencia_hasta         datetime     not null,
    imagen_frente          blob         not null,
    imagen_atras           blob         not null,
    estado_activo          tinyint(1)   not null,
    created_at             timestamp    null,
    updated_at             timestamp    null,
    tipo_identificacion_id int unsigned not null,
    user_id                int unsigned not null,
    constraint user_identificaciones_tipo_identificacion_id_foreign
        foreign key (tipo_identificacion_id) references tipo_identificaciones (id),
    constraint user_identificaciones_user_id_foreign
        foreign key (user_id) references users (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists vehiculos
(
    id                int unsigned auto_increment
        primary key,
    numero_serie      varchar(40)  not null,
    capacidad         varchar(20)  not null,
    requiere_caja     tinyint(1)   not null,
    estado_activo     tinyint(1)   not null,
    numero_economico  varchar(15)  not null,
    anio              int          not null,
    placa_vehiculo_id int unsigned not null,
    marca_vehiculo_id int unsigned not null,
    tipo_unidad_id    int unsigned not null,
    modelo_id         int unsigned not null,
    base_id           int unsigned not null,
    created_at        timestamp    null,
    updated_at        timestamp    null,
    constraint vehiculos_base_id_foreign
        foreign key (base_id) references bases (id),
    constraint vehiculos_marca_vehiculo_id_foreign
        foreign key (marca_vehiculo_id) references marca_vehiculos (id),
    constraint vehiculos_modelo_id_foreign
        foreign key (modelo_id) references modelos (id),
    constraint vehiculos_placa_vehiculo_id_foreign
        foreign key (placa_vehiculo_id) references placa_vehiculos (id),
    constraint vehiculos_tipo_unidad_id_foreign
        foreign key (tipo_unidad_id) references tipo_unidad_vehiculos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists aplicacion_checklists
(
    id                       int unsigned auto_increment
        primary key,
    checklist_instrumento_id int unsigned     not null,
    vehiculo_id              int unsigned     not null,
    user_id                  int unsigned     not null,
    fecha_hora               datetime         not null,
    comentario_general       varchar(255)     not null,
    estado_activo            double default 1 not null,
    created_at               timestamp        null,
    updated_at               timestamp        null,
    constraint aplicacion_checklists_checklist_instrumento_id_foreign
        foreign key (checklist_instrumento_id) references checklist_instrumentos (id),
    constraint aplicacion_checklists_user_id_foreign
        foreign key (user_id) references users (id),
    constraint aplicacion_checklists_vehiculo_id_foreign
        foreign key (vehiculo_id) references vehiculos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists checklist_aplicacion_respuestas
(
    id                      int unsigned auto_increment
        primary key,
    respuesta               varchar(20)          not null,
    comentario              varchar(255)         not null,
    estado_activo           tinyint(1) default 1 not null,
    aplicacion_checklist_id int unsigned         not null,
    created_at              timestamp            null,
    updated_at              timestamp            null,
    constraint checklist_aplicacion_respuestas_aplicacion_checklist_id_foreign
        foreign key (aplicacion_checklist_id) references aplicacion_checklists (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists checklist_respuesta_issues
(
    id                          int unsigned auto_increment
        primary key,
    chk_aplicacion_respuesta_id int unsigned                                     not null,
    comentario_issue            varchar(255)                                     not null,
    prioridad                   enum ('Normal', 'Baja', 'Alta') default 'Normal' not null,
    comentario_solucion         varchar(255)                                     not null,
    estado_activo               tinyint(1)                      default 1        not null,
    created_at                  timestamp                                        null,
    updated_at                  timestamp                                        null,
    constraint checklist_respuesta_issues_chk_aplicacion_respuesta_id_foreign
        foreign key (chk_aplicacion_respuesta_id) references checklist_aplicacion_respuestas (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists seguro_vehiculos
(
    id             int unsigned auto_increment
        primary key,
    numero_poliza  varchar(20)          not null,
    vigencia_hasta date                 not null,
    aseguradora_id int unsigned         not null,
    estado_activo  tinyint(1) default 1 not null,
    descripcion    varchar(255)         not null,
    vehiculo_id    int unsigned         not null,
    created_at     timestamp            null,
    updated_at     timestamp            null,
    constraint seguro_vehiculos_aseguradora_id_foreign
        foreign key (aseguradora_id) references aseguradoras (id),
    constraint seguro_vehiculos_vehiculo_id_foreign
        foreign key (vehiculo_id) references vehiculos (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists viaje_asignacion_detalles
(
    id                    int unsigned auto_increment
        primary key,
    origen                varchar(100) not null,
    destino               varchar(100) not null,
    hora_estimada_salida  datetime     not null,
    hora_estimada_llegada datetime     not null,
    carga                 varchar(100) not null,
    cantidad              varchar(100) not null,
    estado_activo         tinyint(1)   not null,
    unidad_medida_id      int unsigned not null,
    created_at            timestamp    null,
    updated_at            timestamp    null,
    constraint viaje_asignacion_detalles_unidad_medida_id_foreign
        foreign key (unidad_medida_id) references unidad_medidas (id)
)
    collate = utf8mb4_unicode_ci;

create table if not exists viaje_asignaciones
(
    id                 int unsigned auto_increment
        primary key,
    nombre             varchar(200) not null,
    descripcion        varchar(255) not null,
    estado_activo      tinyint(1)   not null,
    fecha_hora         datetime     not null,
    origen             varchar(100) not null,
    destino            varchar(100) not null,
    aplicacion_rasp_id int unsigned not null,
    user_id            int unsigned not null,
    viaje_1_id         int unsigned not null,
    viaje_2_id         int unsigned not null,
    viaje_3_id         int unsigned not null,
    viaje_4_id         int unsigned not null,
    viaje_5_id         int unsigned not null,
    viaje_6_id         int unsigned not null,
    created_at         timestamp    null,
    updated_at         timestamp    null,
    constraint viaje_asignaciones_aplicacion_rasp_id_foreign
        foreign key (aplicacion_rasp_id) references aplicacion_rasp (id),
    constraint viaje_asignaciones_user_id_foreign
        foreign key (user_id) references users (id)
)
    collate = utf8mb4_unicode_ci;

