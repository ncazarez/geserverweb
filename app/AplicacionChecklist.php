<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AplicacionChecklist extends Model
{
    protected  $fillable=[
        'checklist_instrumento_id',
        'checklist_reactivo_id',
        'vehiculo_id',
        'user_id',
        'fecha_hora',
        'comentario_general',
        'estado_activo'
    ];

    public function checklistinstrumento(){
        return $this->hasMany(ChecklistInstrumento::class);
    }

    public function vehiculo(){
        return $this->belongsTo(Vehiculo::class);
    }

    public function usuario(){
        return $this->belongsTo(User::class);
    }

    public function checklist_aplicacion_respuesta(){
        return $this->hasMany(ChecklistAplicacionRespuesta::class);
    }
}
