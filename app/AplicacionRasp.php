<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AplicacionRasp extends Model
{
    protected $fillable=[
        'numero_serie_banda',
        'numero_serie',
        'descripcion',
        'estado_activo',
    ];

    public function viajesAsignados(){
        return $this->hasMany(ViajeAsignacion::class);
    }
}
