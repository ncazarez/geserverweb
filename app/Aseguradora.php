<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aseguradora extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo',
        'descripcion'
    ];
}
