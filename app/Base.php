<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo',
        'region_id'
    ];

    public function vehiculos(){
        $this->hasMany(Vehiculo::class);
    }

    public function region(){
        return $this->belongsTo(Region::class);
    }
}
