<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaReactivo extends Model
{
    protected $fillable=[
        'nombre',
        'descripcion',
        'estado_activo'
    ];

    public function subcategorias(){
        return $this->hasMany(SubCategoriaReactivo::class);
    }
}
