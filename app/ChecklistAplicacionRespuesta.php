<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistAplicacionRespuesta extends Model
{
    public $fillable=[
        'aplicacion_checklist_id',
        'respuesta',
        'comentario',
        'estado_activo'
    ];

    public function aplicacion_checklist(){
        return $this->belongsTo(AplicacionChecklist::class);
    }

    public function checklist_respuesta_issue(){
        return $this->hasMany(ChecklistRespuestaIssues::class);
    }

}
