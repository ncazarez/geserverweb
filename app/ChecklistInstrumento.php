<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistInstrumento extends Model
{
    protected $fillable=[
        'folio_instrumento',
        'nombre',
        'descripcion',
        'estado_activo',
        'modulo_id'
    ];

    public function modulo(){
        return $this->belongsTo(Modulos::class);
    }

    public function checklist_reactivos(){
        return $this->hasMany(ChecklistReactivos::class);
    }

    public function aplicacion_checklist(){
        return $this->hasMany(AplicacionChecklist::class);
    }
}
