<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistReactivos extends Model
{
    protected $fillable=[
        'reactivo_id',
        'checklist_instrumento_id',
        'estado_activo'
    ];

    public function reactivo(){
        return $this->belongsTo(Reactivo::class);
    }

    public function checklist_instrumento(){
        return $this->belongsTo(ChecklistInstrumento::class);
    }



}
