<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChecklistRespuestaIssues extends Model
{

    protected $fillable=[
        'chk_aplicacion_respuesta_id',
        'comentario_issue',
        'prioridad',
        'comentario_solucion',
        'estado_activo'
    ];

    public function checklist_aplicacion_respuesta(){
        return $this->belongsTo(ChecklistAplicacionRespuesta::class);
    }
}
