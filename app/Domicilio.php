<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $fillable=[
        'calle',
        'entre',
        'numero',
        'codigo_postal',
        'colonia',
        'ciudad',
        'ciudad',
        'municipio',
        'estado_activo',
        'estado_territorio_id',
        'usuarios_id',
        'unidad_negocio_id'
    ];

    public function estado(){
        return $this->belongsTo(EstadosTerritorio::class);
    }
}
