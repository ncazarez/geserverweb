<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstadosTerritorio extends Model
{
    protected $fillable=[
        'nombre','siglas','estado_activo'
    ];

    public function placaVehiculos(){
        return $this->hasMany(PlacaVehiculos::class);
    }

    public function domicilios(){
        return $this->hasMany(Domicilio::class);
    }

}
