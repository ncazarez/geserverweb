<?php

namespace App\Http\Controllers\Auth;

use App\Core\Log;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest',['only'=>'showFormLogin']);
    }


    public function showFormLogin(){
        return view('auth.index');
    }


    public function login(Request $request){

        $this->validate(request(),[
            $this->user_name()=>'email|required|string',
            'password'=>'required|string'
        ]);

        $credentials = $request->only($this->user_name(), 'password');

        if( Auth::attempt($credentials)){
            return redirect()->route('dashboard');
        }

        return back()->withErrors([$this->user_name()=>trans('auth.failed')])
            ->withInput(request(['email']));
    }

    public function logout(){
        Auth::logout();
        return redirect('/');
       // return redirect()->route('dashboard');
    }

    public function user_name(){
        return 'correo_electronico';
    }
}
