<?php

namespace App\Http\Controllers\Checklist;

use App\Core\Log;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\VehiculoStoreRequest;
use App\PlacaVehiculos;
use App\Repositories\AseguradoraRepository;
use App\Repositories\BaseRepository;
use App\Repositories\EstadoTerritorioRepository;
use App\Repositories\MarcaVehiculoRepository;
use App\Repositories\RegionRepository;
use App\Repositories\TipoUnidadRepository;
use App\Repositories\VehiculoRepository;
use App\SeguroVehiculo;
use App\Validations\VehiculoValidation;
use App\Vehiculo;

class ChecklistController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('checklist.index');
    }


    public function vehiculos(){
        return view('checklist.veiculos.index')->with([
            'vehiculos'=>(new VehiculoRepository())->listar()
        ]);
    }

    public function nvehiculo()
    {
        return view('checklist.veiculos.nuevo')->with([
            'regiones' => (new RegionRepository())->listar(),
            'marcas' => (new MarcaVehiculoRepository())->listar(),
            'tipos' => (new TipoUnidadRepository())->listar(),
            'estados' => (new EstadoTerritorioRepository())->listar(),
            'aseguradoras' => (new AseguradoraRepository())->listar()
        ]);
    }

    public function ObtenerModelo()
    {
        $marca = (new MarcaVehiculoRepository())->obtener($_POST['id']);
        $rh = new ResponseHelper();
        if (is_object($marca)) {
            $rh->setResponse(true);
            $rh->result = $marca->modelos;
        }

        print_r(json_encode($rh));
    }

    public function CargarBases()
    {
        $base = (new RegionRepository())->obtener($_POST['id']);
        $rh = new ResponseHelper();
        if (is_object($base)) {
            $rh->setResponse(true);
            $rh->result = $base->bases;
        }

        print_r(json_encode($rh));
    }


    public function guardarvehiculo(VehiculoStoreRequest $request)
    {
        $request->validate();

       $modelV = new Vehiculo();
        //$modelV->region_id = $_POST['region'];

        $modelV->base_id = $_POST['base'];
        $modelV->numero_economico = $_POST['neconomico'];
        $modelV->numero_serie = $_POST['nserie'];
        $modelV->marca_vehiculo_id = $_POST['marca'];
        $modelV->modelo_id = $_POST['modelo'];
        $modelV->anio = $_POST['anio'];
        $modelV->tipo_unidad_id = $_POST['tipo-vehiculo'];
        $modelV->capacidad = $_POST['capacidad'];
        $modelV->requiere_caja = $_POST['requiere-caja'];
        $modelV->estado_activo = $_POST['estado-activo'];


        $modelP = new PlacaVehiculos();
        $modelP->num_placa = $_POST['placa'];
        $modelP->tarjeta_circulacion = $_POST['circulacion'];
        $modelP->vigencia_desde = $_POST['vdesde'];
        $modelP->vigencia_hasta = $_POST['vhasta'];
        $modelP->estado_territorio_id = $_POST['estado'];
        $modelP->estado_activo=1;

        $modelP->imagen_placa= $request->file('file')->store('public/placas');

        /*if (!empty($_FILES)) {
            $tmpDate = round(microtime(true));
            $tmp = "PLAC-" . $tmpDate;
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
            try {
                move_uploaded_file(
                    $_FILES['file']['tmp_name'],
                    public_path("img/placas/") . $tmp . "." . $ext
                );
                $modelP->imagen_placa=$tmp . "." . $ext;
            } catch (\Exception $e) {
                Log::error(ChecklistController::class, $e->getMessage() . " " . $e->getLine());
            }
        } */

        $modelS = new SeguroVehiculo();

        $modelS->numero_poliza = $_POST['poliza'];
        $modelS->vigencia_hasta = $_POST['vigenciahasta'];
        $modelS->aseguradora_id = $_POST['aseguradora'];
        $modelS->estado_activo = $_POST['activoa'];
        $modelS->descripcion = $_POST['descripcion'];

        $rh = (new VehiculoRepository())->guardar($modelV, $modelP, $modelS);

        if ($rh->response) {
           // $rh->href = 'vehiculos';
            $rh->href='qrcode/'.$rh->result->id;
        }




        print_r(json_encode($rh));

    }


    public function qrcode($id=0){
        if($id>0) {
            return view('checklist.veiculos.qrcode')->with([
                'vehiculo' => (new VehiculoRepository())->obtener($id)
            ]);
        }else{
            return redirect()->route('vehiculos');
        }
    }

    public function vvehiculo($id=0){
        if($id>0){
            return view('checklist.veiculos.ver_vehiculo')->with([
                'vehiculo'=>(new VehiculoRepository())->obtener($id),
                'regiones' => (new RegionRepository())->listar(),
                'marcas' => (new MarcaVehiculoRepository())->listar(),
                'tipos' => (new TipoUnidadRepository())->listar(),
                'estados' => (new EstadoTerritorioRepository())->listar(),
                'aseguradoras' => (new AseguradoraRepository())->listar()
            ]);
        }else{
            return redirect()->route('vehiculos');
        }
    }

}
