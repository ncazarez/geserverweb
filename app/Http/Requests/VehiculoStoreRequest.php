<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehiculoStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'region' => 'required',
            'base' => 'required',
            'neconomico' => 'required',
            'nserie' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'anio' => 'required',
            'tipo-vehiculo' => 'required',
            'capacidad' => 'required',
            'vdesde' => 'required|date',
            'vhasta' => 'required|date|after:vdesde',
            'estado' => 'required',
            'circulacion' => 'required',
            'placa' => 'required',
            'descripcion' => 'required',
            'aseguradora' => 'required',
            'vigenciahasta' => 'required|date',
            'poliza' => 'required',
            'file' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:6048'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'region.required' => 'Campo requerido',
            'base.required' => 'Campo requerido',
            'neconomico.required' => 'Campo requerido',
            'nserie.required' => 'Campo requerido',
            'marca.required' => 'Campo requerido',
            'modelo.required' => 'Campo requerido',
            'anio.required' => 'Campo requerido',
            'tipo-vehiculo.required' => 'Campo requerido',
            'capacidad.required' => 'Campo requerido',

            'file.required' => 'Debe ingresar una imagen',
            'file.mimes' => 'Solamente se aceptan archivos jpeg,png,jpg,gif,svg',
            'file.max' => 'Los archivos deben tener un maximo de 6M',


            'vhasta.required' => 'Campo requerido',
            'vhasta.date' => 'No tiene el formato correcto',
            'vhasta.after' => 'No puede ser inferior a la fecha de inicio',

            'vdesde.required' => 'Campo requerido',
            'estado.required' => 'Campo requerido',
            'circulacion.required' => 'Campo requerido',
            'placa.required' => 'Campo requerido',

            'poliza.required' => 'Campo requerido',
            'vigenciahasta.required' => 'Campo requerido',
            'aseguradora.required' => 'Campo requerido',
            'descripcion.required' => 'Campo requerido',


        ];
    }
}
