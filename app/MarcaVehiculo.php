<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarcaVehiculo extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo'
    ];


    public function modelos(){
        return $this->hasMany(Modelo::class);
    }
}
