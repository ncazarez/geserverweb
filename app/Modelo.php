<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo',
        'marca_vehiculo_id'
    ];

    public function marca(){
        return $this->belongsTo(MarcaVehiculo::class);
    }

    public function vehiculoss(){
        return $this->hasMany(Vehiculo::class);
    }
}
