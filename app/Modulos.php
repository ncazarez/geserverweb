<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modulos extends Model
{
    protected $fillable=[
        'nombre',
        'descripcion',
        'estado_activo'
    ];

    public function permisos(){
        return $this->hasMany(Permisos::class);
    }

    public function checklistinstrumentos(){
        return $this->hasMany(ChecklistInstrumento::class);
    }
}
