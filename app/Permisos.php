<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permisos extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo',
        'modulo_id'
    ];
    public function modulo(){
        return $this->belongsTo(Modulos::class);
    }

    public function permisosNegados(){
        return $this->hasMany(PermisosNegados::class);
    }
}
