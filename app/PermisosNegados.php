<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermisosNegados extends Model
{
    protected $fillable=[
        'permiso_id',
        'rol_id'
    ];

    public function permiso(){
        return $this->belongsTo(Permisos::class);
    }

    public function rol(){
        return $this->belongsTo(Roles::class);
    }
}
