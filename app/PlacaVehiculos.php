<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlacaVehiculos extends Model
{
    protected $fillable=[
        'num_placa',
        'targeta_circulacion',
        'imagen_placa',
        'valida_desde',
        'valida_hasta',
        'estado_territorio_id',
    ];

    public function estadoTerritorio(){
        return $this->belongsTo(EstadosTerritorio::class);
    }
}
