<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reactivo extends Model
{
    protected $fillable=[
        'texto_respuesta',
        'tipo_respuesta',
        'comentario',
        'critico',
        'texto_reactivo',
        'estado_activo',
        'subcategoriaReactivo'
    ];

    public function subcategoria(){
        return $this->belongsTo(SubCategoriaReactivo::class);
    }


    public function checklist_reactivos(){
        return $this->hasMany(ChecklistReactivos::class);
    }
}
