<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo',
    ];

    public function bases(){
        return $this->hasMany(Base::class);
    }
}
