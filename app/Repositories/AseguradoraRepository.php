<?php


namespace App\Repositories;


use App\Aseguradora;
use Illuminate\Database\Eloquent\Collection;

class AseguradoraRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new Aseguradora();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos=$this->model->orderBy('nombre')->get();
        } catch (\Exception $e) {
            \Log::error(RegionRepository::class, $e->getMessage());
        }
        return $datos;
    }

}