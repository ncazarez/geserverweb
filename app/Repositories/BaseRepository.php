<?php


namespace App\Repositories;


use App\Base;
use Illuminate\Database\Eloquent\Collection;

class BaseRepository
{
    private $model;

    public function __construct()
    {
        $this->model=new Base();
    }

    public function listar(): Collection
{
    $datos = [];
    try {
        $datos=$this->model->orderBy('nombre')->get();
    } catch (\Exception $e) {
        \Log::error(RegionRepository::class, $e->getMessage());
    }
    return $datos;
}

    public function obtenerByRegion($id): Collection
    {
        $datos = [];
        try {
            $datos=$this->model->where('region_id',$id)
                ->orderBy('nombre')->get();
        } catch (\Exception $e) {
            \Log::error(RegionRepository::class, $e->getMessage());
        }
        return $datos;
    }


}