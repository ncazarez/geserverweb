<?php


namespace App\Repositories;


use App\EstadosTerritorio;
use Illuminate\Database\Eloquent\Collection;

class EstadoTerritorioRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new EstadosTerritorio();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos=$this->model->orderBy('nombre')->get();
        } catch (\Exception $e) {
            \Log::error(RegionRepository::class, $e->getMessage());
        }
        return $datos;
    }

}