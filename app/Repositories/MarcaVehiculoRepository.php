<?php


namespace App\Repositories;


use App\Core\Log;
use App\MarcaVehiculo;
use Illuminate\Database\Eloquent\Collection;

class MarcaVehiculoRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new MarcaVehiculo();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos = $this->model->where('estado_activo',true)->orderBy('nombre')->get();
        } catch (\Exception $e) {
            Log::error(RegionRepository::class, $e->getMessage());
        }
        return $datos;
    }


    public function obtener($id): ?MarcaVehiculo
    {
        $modelo = null;
        try {
            $modelo=$this->model->find($id);

        } catch (\Exception $e) {
            Log::error(MarcaVehiculoRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $modelo;
    }


}