<?php


namespace App\Repositories;


use App\Region;
use Illuminate\Database\Eloquent\Collection;

class RegionRepository
{
    private $model;

    public function __construct()
    {
        $this->model = new Region();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos = $this->model->orderBy('nombre')->get();
        } catch (\Exception $e) {
            \Log::error(RegionRepository::class, $e->getMessage());
        }
        return $datos;
    }

    public function obtener($id): ?Region
    {
        $model = null;
        try {
            $model=$this->model->find($id);
        } catch (\Exception $e) {
            Log::error(RegionRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $model;
    }


}