<?php


namespace App\Repositories;


class SubCategoriaReactivoRepository
{
private $model;

    public function __construct()
    {
        $this->model=new CategoriaReactivo();
    }

    public function listar(){
        $datos=[];
        try{
            $datos=$this->model->orderBy('nombre')->get();
        }catch (\Exception $e){
            Log::error(CategoriaReactivo::class,$e->getMessage() . " Linea: " . $e->getLine());
        }
        return $datos;
    }

    public function guardar($nModel){
        $rh=new ResponseHelper();
        try{
            $this->model=$nModel;
            if(!empty($nModel->id)){
                $this->model->exists=true;
            }
            $this->model->save();
            $rh->setResponse();
        }catch (\Exception $e){
            Log::error(CategoriaReactivo::class,$e->getMessage() . " Linea: " . $e->getLine());
        }
        return $rh;
    }

    public function obtener($id){
        $dato=null;
        try{
            $dato=$this->model->find($id);
        }catch (\Exception $e){
            Log::error(CategoriaReactivo::class,$e->getMessage() . " Linea: " . $e->getLine());
        }
        return $dato;
    }

    public function borrrar($id){
        $rh=new ResponseHelper();
        $dato=$this->obtener($id);
        try{
            $dato->estado_activo=false;
            $dato->save();
            $rh->setResponse(true);
        }catch (\Exception $e){
            Log::error(CategoriaReactivo::class,$e->getMessage() . " Linea: " . $e->getLine());
        }
        return $rh;
    }

}