<?php


namespace App\Repositories;


use App\Region;
use App\TipoUnidadVehiculo;
use Illuminate\Database\Eloquent\Collection;

class TipoUnidadRepository
{

    private $model;

    public function __construct()
    {
        $this->model = new TipoUnidadVehiculo();
    }

    public function listar(): Collection
    {
        $datos = [];
        try {
            $datos=$this->model->orderBy('tipo_unidad')->get();
        } catch (\Exception $e) {
            \Log::error(RegionRepository::class, $e->getMessage());
        }
        return $datos;
    }
}