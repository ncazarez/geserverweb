<?php


namespace App\Repositories;


use App\Core\Log;
use App\Helpers\ResponseHelper;
use App\PlacaVehiculos;
use App\SeguroVehiculo;
use App\Vehiculo;
use http\Client\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class VehiculoRepository
{
    private $model;
    private $modelPlaca;
    private $modelSeguro;

    public function __construct()
    {
        $this->model = new Vehiculo();
        $this->modelPlaca = new PlacaVehiculos();
        $this->modelSeguro = new SeguroVehiculo();
    }

    public function listar(): ?Collection
    {
        $datos = [];
        try {
            $datos = $this->model->where('estado_activo', 1)->get();
        } catch (\Exception $e) {
            Log::error(VehiculoRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $datos;
    }

    public function guardar($modelV, $modelP = null, $modelS = null): ResponseHelper
    {
        $rh = new ResponseHelper();
        DB::beginTransaction();

        try {



            $this->model = $modelV;

            if ($modelP != null) {
                $this->modelPlaca = $modelP;
                $this->modelPlaca->save();
                $this->model->placa_vehiculo_id = $this->modelPlaca->id;
            }

            if (!empty($modelV->id)) {
                $this->model->exists = true;
            }
            $this->model->save();

            if ($modelS != null) {
                $this->modelSeguro = $modelS;
                $this->modelSeguro->vehiculo_id = $this->model->id;
                $this->modelSeguro->save();
            }

            DB::commit();
            $rh->setResponse(true);
            $rh->result = $this->model;

        } catch (\Exception $e) {
            Log::error(VehiculoRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
            DB::rollback();
        }
        return $rh;
    }


    public function obtener($id):? Vehiculo
    {
        $model = null;
        try {
            $model=$this->model->find($id);
        } catch (\Exception $e) {
            Log::error(VehiculoRepository::class, $e->getMessage() . " Linea: " . $e->getLine());
        }
        return $model;
    }


}