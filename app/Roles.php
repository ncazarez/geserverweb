<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    protected $fillable = [
        'nombre',
        'estado_activo'
    ];

    public function permisosNegados()
    {
        return $this->hasMany(PermisosNegados::class);
    }

    public function usuarios(){
        return $this->hasMany(User::class);
    }
}
