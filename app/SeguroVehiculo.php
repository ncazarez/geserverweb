<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeguroVehiculo extends Model
{
    protected $fillable=[
        'numero_poliza',
        'vigencia_hasta',
        'aseguradora_id',
        'estado_activo',
        'descripcion',
        'vehiculo_id',
    ];

    public function vehiculo(){
        return $this->belongsTo(Vehiculo::class);
    }
}
