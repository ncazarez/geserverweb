<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoriaReactivo extends Model
{
    protected  $fillable=[
        'nombre',
        'descripcion',
        'esado_activo',
        'categoria_id'
    ];

    public function categoria(){
        return $this->belongsTo(CategoriaReactivo::class);
    }

    public function reactivos(){
        return $this->hasMany(Reactivo::class);
    }
}
