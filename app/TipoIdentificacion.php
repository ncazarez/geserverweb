<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoIdentificacion extends Model
{
    protected  $fillable=[
        'nombre','estado_activo'
    ];

    public function identificaciones(){

    return $this->hasMany(UserIdentificacion::class);
    }
}
