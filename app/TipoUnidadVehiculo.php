<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoUnidadVehiculo extends Model
{
    protected $fillable=[
        'tipo_unidad'
    ];
}
