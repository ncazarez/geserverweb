<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadMedida extends Model
{
    protected $fillable=[
        'nombre',
        'estado_activo'
    ];

    public function viajesAsignados(){
        return $this->hasMany(ViajeAsignacionDetalle::class);
    }
}
