<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    const USUARIO_GENERO_HOMBRE = 'hombre';
    const USUARIO_GENERO_MUJER = 'mujer';
    const USUARIO_GENERO_OTRO = 'otro';

    const USUARIO_TIPO_INTERNO = 'interno';
    const USUARIO_TIPO_EXTERNO = 'externo';



    protected $fillable = [
        'correo_electronico',
        'password',
        'nombres',
        'a_paterno',
        'a_materno',
        'genero',
        'celular',
        'puesto',
        'tipo_empleado',
        'estado_activo',
        'rol_id'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol(){
        return $this->belongsTo(Roles::class);
    }

    public function identificaciones(){
        return $this->hasMany(UserIdentificacion::class);
    }
}
