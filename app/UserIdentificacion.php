<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIdentificacion extends Model
{
    protected  $fillable=[
        'folio',
        'vigencia_desde',
        'vigencia_hasta',
        'imagen_frente',
        'imagen_atras',
        'estado_activo',
        'user_id'
    ];

    public function tipoidentificacion(){
        return $this->belongsTo(TipoIdentificacion::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
