<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{
    protected $fillable=[
        'numero_serie',
        'modelo_id',
        'anio',
        'capacidad',
        'requiere_caja',
        'estado_activo',
        'placa_id',
        'marca_id',
        'numero_economico',
        'tipo_unidad_id',
        'base_id'
    ];

    public function tipoUnidad(){
        return $this->belongsTo(TipoUnidadVehiculo::class);
    }
    public function marcaVehiculo(){
        return $this->belongsTo(MarcaVehiculo::class);
    }
    public function placaVehiculo(){
        return $this->belongsTo(PlacaVehiculos::class);
    }

    public function modelo(){
        return $this->belongsTo(Modelo::class);
    }



    public function base(){
        return $this->belongsTo(Base::class);
    }

    public function seguros(){
        return $this->hasMany(SeguroVehiculo::class);
    }
}
