<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViajeAsignacion extends Model
{
    protected $fillable=[
        'nombre',
        'descripcion',
        'estado_activo',
        'fecha_hora',
        'origen',
        'destino',
        'destino',
        'user_id',
        'aplicacion_rasp_id',
        'viaje_1_id',
        'viaje_2_id',
        'viaje_3_id',
        'viaje_4_id',
        'viaje_5_id',
        'viaje_6_id'
    ];

    public function chofer(){
        return $this->belongsTo(User::class);
    }



    public function aplicacion(){
        return $this->belongsTo(AplicacionRasp::class);
    }
}
