<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViajeAsignacionDetalle extends Model
{
 protected $fillable=[
     'origen',
     'destino',
     'hora_estimada_salida',
     'hora_estimada_llegada',
     'carga',
     'cantidad',
     'estado_activo',
     'unidad_medida_id'
 ];

 public function unidadMedida(){
     return $this->belongsTo(UnidadMedida::class);
 }
}
