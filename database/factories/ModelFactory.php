<?php






/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Base;
use App\EstadosTerritorio;
use App\MarcaVehiculo;
use App\Modulos;
use App\Region;
use App\Roles;
use App\TipoIdentificacion;
use App\User;
use App\UserIdentificacion;
use Faker\Generator;

$factory->define(App\Modulos::class, function (Faker\Generator $faker) {

    return [
        'nombre' => $faker->randomElement(['WEB','APP','DASHBOARD']),
        'descripcion'=>$faker->text(250),
        'estado_activo'=>$faker->boolean,
    ];
});

$factory->define(App\Roles::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement(["CHOFER","SUPERVISOR","ADMINISTRADOR","DEVELOPER"]),
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $id=Roles::all()->random()->id;
    return [
        'nombre' => $faker->name,
        'apaterno'=>$faker->firstName,
        'amaterno'=>$faker->lastName,
        'correo_electronico' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'genero'=>$faker->randomElement([User::USUARIO_GENERO_HOMBRE,User::USUARIO_GENERO_MUJER, User::USUARIO_GENERO_OTRO]),
        'celular'=>$faker->phoneNumber,
        'puesto'=>$faker->randomElement(['JEFE DE LOGISTICA','SUPERVISOR ZONA HILLO', 'TRANSPORTISTA']),
        'tipo_empleado'=>$faker->randomElement([User::USUARIO_TIPO_INTERNO,User::USUARIO_TIPO_EXTERNO]),
        'esta_activo'=>$faker->randomElement([1,0]),
        'rol_id'=>$id,
    ];
});



$factory->define(App\TipoIdentificacion::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->text(20),
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});

$factory->define(UserIdentificacion::class, function (Faker\Generator $faker) {
    $tindentificacion=TipoIdentificacion::all()->get()->random();
    $usuario=User::all()->get()->random();
    return [
        'folio' => $faker->text(20),
        'vigencia_desde'=>$faker->dateTime(),
        'vigencia_hasta'=>$faker->dateTime(),
        'estado_activo'=>$faker->randomElement([true,false]),
        'tipoidentificacion_id'=>$tindentificacion->id,
        'user_id'=>$usuario->id,

    ];
});



$factory->define(App\EstadosTerritorio::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement(["SONORA","SINALOA","CHIHUAHUA","NAYARIT","BAJACALIFORNIA SUR", "BAJACALIFORNIA NORTE","DURANGO"]),
        'siglas' => 'MX',
        'pais' =>'MX',
        'estado_activo'=>$faker->randomElement(true,false),
    ];
});

$factory->define(App\Base::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement(["ENCENADA","GUADALAJARA","HERMOSILLO","IXTRAN DEL RIO","LA PAZ"]),
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});

$factory->define(App\Region::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement(["CENTRO","NOROESTE","PACIFICO","SUROESTE"]),
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});

$factory->define(App\MarcaVehiculo::class, function (Faker\Generator $faker) {
    return [
        'nombre' => $faker->randomElement(["Freighliner","International","Kenworth"]),
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});

$factory->define(App\Modelo::class, function (Faker\Generator $faker) {
    $marca=MarcaVehiculo::all()->random();
    return [
        'marca_vehiculo_id'=>$marca->id,
        'nombre' => $faker->randomElement(['MODELO-1','MODELO-2','MODELO-3','MODELO-4','MODELO-5','MODELO-6','MODELO-7','MODELO-8']),
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});



/*
$factory->define(App\Domicilio::class, function (Faker\Generator $faker) {
    $usuario=User::all()->get()->random();
    $estado=EstadosTerritorio::all()->get()->random();
    return [
        'calle'=>$faker->text(30),
        'entre'=>$faker->text(30),
        'numero'=>$faker->randomNumber(),
        'codigo_postal'=>$faker->countryCode,
        'ciudad'=>$faker->city,
        'municipio'=>$faker->country,
        'usuario_id'=>$usuario->id,
        'estado_territorio_id'=>$estado->id,
        'estado_activo'=>$faker->randomElement([true,false]),
    ];
});

*/




