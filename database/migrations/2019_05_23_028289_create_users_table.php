<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('correo_electronico',50)->unique();
            $table->string('password',100);
            $table->string('nombre',80);
            $table->string('apaterno',80);
            $table->string('amaterno',80);
            $table->string('genero')->default(\App\User::USUARIO_GENERO_HOMBRE);
            $table->string('celular',100);
            $table->integer('rol_id')->unsigned();
            $table->string('puesto',50);
            $table->string('tipo_empleado')->default(\App\User::USUARIO_TIPO_INTERNO);
            $table->boolean('esta_activo')->defaul(true);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('rol_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
