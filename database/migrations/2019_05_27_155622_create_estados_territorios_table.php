<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosTerritoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estados_territorios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string('nombre',50);
            $table->string('siglas',20);
            $table->boolean('estado_activo',true);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estados_territorios');
    }
}
