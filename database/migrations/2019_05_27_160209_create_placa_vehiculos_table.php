<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacaVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placa_vehiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->boolean('estado_activo',true);
            $table->string('num_placa',20);
            $table->string('tarjeta_circulacion',20);
            $table->string('imagen_placa');
            $table->date('vigencia_desde');
            $table->date('vigencia_hasta');
            $table->integer('estado_territorio_id')->unsigned();


            $table->foreign('estado_territorio_id')->references('id')->on('estados_territorios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placa_vehiculos');
    }
}
