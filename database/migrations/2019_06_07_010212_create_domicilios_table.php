<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDomiciliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('calle',50);
            $table->string('entre',100);
            $table->string('numero',8);
            $table->string('codigo_postal',5);
            $table->string('colonia',50);
            $table->string('ciudad',50);
            $table->string('municipio',50);
            $table->boolean('estado_activo',true);
            $table->integer('estado_territorio_id')->unsigned();
            $table->integer('usuarios_id')->unsigned();
            $table->integer('unidad_negocio_id')->unsigned();

            $table->timestamps();

            $table->foreign('estado_territorio_id')->references('id')->on('estados_territorios');
            $table->foreign('usuarios_id')->references('id')->on('users');

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilios');
    }
}
