<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_serie',40);

            $table->string('capacidad',20);
            $table->boolean('requiere_caja',false);
            $table->boolean('estado_activo',true);
            $table->string('numero_economico',15);

            $table->integer('anio');

            $table->integer('placa_vehiculo_id')->unsigned();
            $table->integer('marca_vehiculo_id')->unsigned();
            $table->integer('tipo_unidad_id')->unsigned();
            $table->integer('modelo_id')->unsigned();
            $table->integer('base_id')->unsigned();


            $table->foreign('placa_vehiculo_id')->references('id')->on('placa_vehiculos');
            $table->foreign('marca_vehiculo_id')->references('id')->on('marca_vehiculos');
            $table->foreign('tipo_unidad_id')->references('id')->on('tipo_unidad_vehiculos');
            $table->foreign('modelo_id')->references('id')->on('modelos');

            $table->foreign('base_id')->references('id')->on('bases');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
}
