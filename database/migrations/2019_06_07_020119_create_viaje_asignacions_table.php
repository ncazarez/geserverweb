<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeAsignacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viaje_asignaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',200);
            $table->string('descripcion');
            $table->boolean('estado_activo',true);
            $table->dateTime('fecha_hora');
            $table->string('origen',100);
            $table->string('destino',100);

            $table->integer('aplicacion_rasp_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->integer('viaje_1_id')->unsigned();
            $table->integer('viaje_2_id')->unsigned();
            $table->integer('viaje_3_id')->unsigned();
            $table->integer('viaje_4_id')->unsigned();
            $table->integer('viaje_5_id')->unsigned();
            $table->integer('viaje_6_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('aplicacion_rasp_id')->references('id')->on('aplicacion_rasp');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viaje_asignacions');
    }
}
