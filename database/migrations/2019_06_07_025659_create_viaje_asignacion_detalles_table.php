<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateViajeAsignacionDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('viaje_asignacion_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origen',100);
            $table->string('destino',100);
            $table->dateTime('hora_estimada_salida');
            $table->dateTime('hora_estimada_llegada');
            $table->string('carga',100);
            $table->string('cantidad',100);

            $table->boolean('estado_activo',true);
            $table->integer('unidad_medida_id')->unsigned();

            $table->foreign('unidad_medida_id')->references('id')->on('unidad_medidas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('viaje_asignacion_detalles');
    }
}
