<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserIdentificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_identificaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio');
            $table->dateTime('vigencia_desde');
            $table->dateTime('vigencia_hasta');
            $table->binary('imagen_frente');
            $table->binary('imagen_atras');
            $table->boolean('estado_activo',true);
            $table->timestamps();

            $table->integer('tipo_identificacion_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('tipo_identificacion_id')->references('id')->on('tipo_identificaciones');
            $table->foreign('user_id')->references('id')->on('users');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_identificaciones');
    }
}
