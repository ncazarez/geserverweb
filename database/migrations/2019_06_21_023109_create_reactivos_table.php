<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReactivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactivos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('texto_respuesta',100);
            $table->string('tipo_respuesta',100);
            $table->string('comentario');
            $table->string('texto_reactivo',100);
            $table->boolean('critico')->default(true);
            $table->boolean('estado_activo')->default(true);

            $table->integer('sub_categoria_reactivo_id')->unsigned();
            $table->timestamps();

            $table->foreign('sub_categoria_reactivo_id')->references('id')->on('sub_categoria_reactivos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactivos');
    }
}
