<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistInstrumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_instrumentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('folio_instrumento',15);
            $table->string('nombre',100);
            $table->string('descripcion');
            $table->boolean('estado_activo')->default(true);
            $table->integer('modulo_id')->unsigned();
            $table->timestamps();

            $table->foreign('modulo_id')->references('id')->on('modulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_instrumentos');
    }
}
