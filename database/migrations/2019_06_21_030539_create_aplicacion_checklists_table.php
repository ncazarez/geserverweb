<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAplicacionChecklistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aplicacion_checklists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('checklist_instrumento_id')->unsigned();
            $table->integer('vehiculo_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('fecha_hora');
            $table->string('comentario_general');
            $table->double('estado_activo')->default(true);

            $table->timestamps();

            $table->foreign('checklist_instrumento_id')->references('id')->on('checklist_instrumentos');
            $table->foreign('vehiculo_id')->references('id')->on('vehiculos');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aplicacion_checklists');
    }
}
