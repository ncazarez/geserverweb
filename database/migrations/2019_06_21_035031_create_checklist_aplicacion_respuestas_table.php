<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChecklistAplicacionRespuestasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_aplicacion_respuestas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('respuesta',20);
            $table->string('comentario');
            $table->boolean('estado_activo')->default(true);
            $table->integer('aplicacion_checklist_id')->unsigned();
            $table->timestamps();

            $table->foreign('aplicacion_checklist_id')->references('id')->on('aplicacion_checklists');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_aplicacion_respuestas');
    }
}
