<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChecklistRespuestaIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checklist_respuesta_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chk_aplicacion_respuesta_id')->unsigned();
            $table->string('comentario_issue');
            $table->enum('prioridad', array('Normal', 'Baja', 'Alta'))->default('Normal');
            $table->string('comentario_solucion');
            $table->boolean('estado_activo')->default(true);
            $table->timestamps();

            $table->foreign('chk_aplicacion_respuesta_id')->references('id')->on('checklist_aplicacion_respuestas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('checklist_respuesta_issues');
    }
}
