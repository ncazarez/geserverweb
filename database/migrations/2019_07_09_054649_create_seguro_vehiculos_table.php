<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeguroVehiculosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguro_vehiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_poliza',20);
            $table->date('vigencia_hasta');
            $table->integer('aseguradora_id')->unsigned();
            $table->boolean('estado_activo')->default(true);
            $table->string('descripcion');
            $table->integer('vehiculo_id')->unsigned();
            $table->timestamps();

            $table->foreign('aseguradora_id')->references('id')->on('aseguradoras');
            $table->foreign('vehiculo_id')->references('id')->on('vehiculos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seguro_vehiculos');
    }
}
