<?php


use App\Roles;
use App\User;
use Illuminate\Database\Seeder;
use App\Modulos;

use App\Region;
use App\Base;
use App\MarcaVehiculo;
use App\Modelo;




class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Modulos::truncate();
        Roles::truncate();
        User::truncate();

        Region::truncate();
        Base::truncate();
        MarcaVehiculo::truncate();
        Modelo::truncate();



        factory(Modulos::class,5)->create();
        factory(Roles::class,4)->create();
        factory(User::class,20)->create();

        factory(Region::class,6)->create();
        factory(Base::class,6)->create();

        factory(MarcaVehiculo::class,4)->create();
        factory(Modelo::class,20)->create();


        //factory(EstadosTerritorio::class,4)->create();
        //factory(Domicilio::class,4)->create();

    }
}
