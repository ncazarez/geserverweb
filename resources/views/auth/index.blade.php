@extends ("layout.login_plantilla")
@section('style')
    <style>
        #panelLogin {
            margin-top: 20px;
        }
    </style>
@endsection
@section('content')

    <div class="row">
        <div id="panelLogin" class="col-md-4 col-md-offset-4">
            <div class="col-md-12">
                <img class="img-responsive"
                     src="{{ asset('img/logos/logo_cemex.png')  }}"
                     alt="Cemex">
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title text-center">Acceso a la Aplicación</h1>
                </div>
                <div class="panel-body">
                    @if (session()->has('flash'))
                        <div class="alert alert-info">{{ session('flash') }}</div>
                    @endif
                    <form method="POST" action="{{ route('login') }}">
                        {{ csrf_field()  }}
                        <div class="form-group {{ $errors->has('correo_electronico') ? 'has-error' : ''  }}">
                            <label for="correo_electronico">Email</label>
                            <input value="{{ old('email') }}" type="email" name="correo_electronico"
                                   class="form-control" placeholder="Ingresa tu email">
                            {!! $errors->first('correo_electronico','<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group {{ $errors->has('password') ? 'has-error' : ''  }}">
                            <label for="password">Contraseña</label>
                            <input type="password" name="password" class="form-control" placeholder="">
                            {!! $errors->first('password','<span class="help-block">:message</span>') !!}
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection