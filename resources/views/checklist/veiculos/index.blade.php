@extends ('layout.checklist.plantilla')

@section('content')

    <div class="container-fluid">
        @include ("layout.partials.menu_controller")

        <h1 class="display-5 text-center text-gray-800 font-weight-bold">CATÁLOGO DE UNIDADES</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed">
                <thead>
                <tr class="text-center">
                    <td>REGION</td>
                    <td>BASE</td>
                    <td>NUM. SER.</td>
                    <td>NUM. ECO.</td>
                    <td>MARCA</td>
                    <td>MODELO</td>
                    <td>AÑO</td>
                    <td>CAPACIDAD</td>
                    <td>Qr-CODE</td>
                </tr>
                </thead>
                <tbody>
                @foreach($vehiculos as $vehiculo)
                    <tr>
                        <td class="align-middle">
                            <a href="{{route('vervehiculo',$vehiculo->id)}}">
                                {{ strtoupper($vehiculo->base->region->nombre)  }}
                            </a>
                        </td>
                        <td class="align-middle">{{ strtoupper($vehiculo->base->nombre)  }}</td>
                        <td class="align-middle">{{ strtoupper( $vehiculo->numero_serie) }}</td>
                        <td class="align-middle">{{ strtoupper( $vehiculo->numero_economico)  }}</td>
                        <td class="align-middle">{{ strtoupper( $vehiculo->marcaVehiculo->nombre) }}</td>
                        <td class="align-middle">{{ strtoupper($vehiculo->modelo->nombre)  }}</td>
                        <td class="align-middle">{{ $vehiculo->anio  }}</td>
                        <td class="align-middle">{{ strtoupper( $vehiculo->capacidad)  }}</td>
                        <td class="align-middle">
                            <a href="{{ route( 'qrcode', $vehiculo->id)  }}" target="_blank">
                                {!! QrCode::size(60)->generate(strtoupper( $vehiculo->numero_economico)); !!}
                            </a>
                        </td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="9" class="text-right">REGISTROS: {{ count($vehiculos)  }}</td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>
@endsection