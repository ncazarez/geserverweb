@extends ("layout.checklist.plantilla")

@section('styles')
    <style>
        hr {
            margin-top: 1rem;
            margin-bottom: 1rem;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.1);
        }
        .validation-message{
            color:#D40000;
        }
        input, textarea{
            text-transform: uppercase;
        }
    </style>
@endsection

@section('content')
    <div id="content">

        @include ("layout.partials.menu_controller")

        <div class="container-fluid">
            <form id="frmRegistro" action="{{ url('gvehiculo') }}" method="post" enctype="multipart/form-data">
                <div class="alert-container"></div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="pb-2 mt-8 mb-4 border-bottom">
                    <h5> Registro de Nuevo Vehículo</h5>
                </div>
                <div class="row">
                    <div class="form-group col-md-2">
                        <label>Región</label>
                        <select name="region" id="region" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach ($regiones as $region)
                                <option value="{{$region->id}}">{{$region->nombre}}</option>
                            @endforeach
                        </select>
                        <span class="validation-message" data-key="region"></span>
                    </div>

                    <div class="form-group col-md-2">
                        <label>Base</label>
                        <select name="base" class="form-control" id="base">
                            <option selected disabled>Elegir..</option>

                        </select>
                        <span class="validation-message" data-key="base"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Número Económico:</label>
                        <input type="text" name="neconomico" class="form-control" placeholder="Número Económico..">
                        <span class="validation-message" data-key="neconomico"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Número de Serie:</label>
                        <input type="text" name="nserie" class="form-control" placeholder="Número de Serie..">
                        <span class="validation-message" data-key="nserie"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Marca</label>
                        <select name="marca" id="marca" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach ($marcas as $marca)
                                <option value="{{$marca->id}}">{{$marca->nombre}}</option>
                            @endforeach
                        </select>
                        <span class="validation-message" data-key="marca"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Modelo</label>
                        <select name="modelo" id="modelo" class="form-control">
                            <option selected disabled>Elegir..</option>
                        </select>
                        <span class="validation-message" data-key="modelo"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Año</label>
                        <select name="anio" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @for ($i = 2020; $i > 1990; $i--)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <span class="validation-message" data-key="anio"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Tipo de Vehículo</label>
                        <select name="tipo-vehiculo" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach ($tipos as $tipo)
                                <option value="{{$tipo->id}}">{{$tipo->tipo_unidad}}</option>
                            @endforeach
                        </select>
                        <span class="validation-message" data-key="tipo-vehiculo"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Capacidad</label>
                        <select name="capacidad" class="form-control">
                            <option selected disabled>Elegir..</option>
                            <option value="1_3">1 a 3 ton.</option>
                            <option value="4_7">4 a 7 ton.</option>
                            <option value="8_12">8 a 12 ton.</option>
                            <option value="13_20">13 a 20 ton.</option>
                        </select>
                        <span class="validation-message" data-key="capacidad"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>¿Requiere Caja?</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input checked class="form-check-input" type="radio" name="requiere-caja"
                                   id="requiere-caja1"
                                   value="1">
                            <label class="form-check-label" for="requiere-caja1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="requiere-caja" id="requiere-caja0"
                                   value="0">
                            <label class="form-check-label" for="requiere-caja0">No</label>
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <label>¿Estado activo?</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input checked class="form-check-input" type="radio" name="estado-activo"
                                   id="estado-activo1"
                                   value="1">
                            <label class="form-check-label" for="estado-activo1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="estado-activo" id="estado-activo0"
                                   value="0">
                            <label class="form-check-label" for="estado-activo0">No</label>
                        </div>
                    </div>
                </div>



                <div class="pb-2 mt-8 mb-4 border-bottom">
                    <h5>Placa de Vehículo</h5>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label>Placa de Vehículo</label>
                        <input type="text" name="placa" placeholder="Placa de Vehículo.." class="form-control">
                        <span class="validation-message" data-key="placa"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Número de Tarjeta Circulación</label>
                        <input type="text" name="circulacion" placeholder="Número de Tarjeta Circulación.."
                               class="form-control">
                        <span class="validation-message" data-key="circulacion"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Estado/Territorio</label>
                        <select name="estado" class="form-control">
                            <option disabled selected>Elegir..</option>
                            @foreach($estados as $estado)
                                <option value="{{$estado->id}}">{{$estado->nombre}}</option>
                            @endforeach
                        </select>
                        <span class="validation-message" data-key="estado"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Vigencia desde:</label>
                        <input type="date" name="vdesde" placeholder="Vigencia desde:" class="form-control">
                        <span class="validation-message" data-key="vdesde"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Vigencia hasta:</label>
                        <input type="date" name="vhasta" placeholder="Vigencia hasta.." class="form-control">
                        <span class="validation-message" data-key="vhasta"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Imagen de la Placa:</label>
                        <input type="file" name="file" class="form-control">
                        <span class="validation-message" data-key="file"></span>
                    </div>
                </div>

                <div class="pb-2 mt-8 mb-4 border-bottom">
                    <h5>Seguro del Vehículo</h5>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label>Número de Póliza de Seguro:</label>
                        <input type="text" name="poliza" placeholder="Número de Póliza de Seguro.."
                               class="form-control">
                        <span class="validation-message" data-key="poliza"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Vigencia Hasta:</label>
                        <input type="date" name="vigenciahasta" placeholder="dd/mm/yyyy" class="form-control">
                        <span class="validation-message" data-key="vigenciahasta"></span>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Aseguradora:</label>
                        <select name="aseguradora" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach($aseguradoras as $aseguradora)
                                <option value="{{$aseguradora->id}}">{{$aseguradora->nombre}}</option>
                            @endforeach
                        </select>
                        <span class="validation-message" data-key="aseguradora"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>¿Activo?</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="activoa" id="activo1" value="1">
                            <label class="form-check-label" for="activo1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="activoa" id="activo0" value="0">
                            <label class="form-check-label" for="activo0">No</label>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <label>Descripción o Comentario de la Póliza:</label>
                        <textarea name="descripcion" placeholder="Descripción o Comentario de la Póliza.."
                                  class="form-control"
                                  rows="5"></textarea>
                        <span class="validation-message" data-key="descripcion"></span>
                    </div>


                </div>

                <hr>
                <div class="row">
                    <button type="submit" class="btn btn-primary ml-auto" style="margin-top: 10px;">
                        <i class="fa fa-save" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            </form>
        </div>

        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Security Checklist Manager &copy; V 1.0</span>
                </div>
            </div>
        </footer>
    </div>
@endsection

@section ('scripts')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });



        $('body').on('submit', '#frmRegistro', function (e) {
            e.preventDefault();
            var form = $(this);
            $('.validation-message').text('');

          var data=new FormData(this);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: data,
                datatype: 'json',
                cache: false,
                contentType: false,
                processData: false,
                success: function (r) {
                    r = JSON.parse(r);
                    if(r.response) {
                        redirect(r.href);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status === 422) {
                        for (var k in jqXHR.responseJSON) {
                            var control = form.find('.validation-message[data-key="' + k + '"]');
                            control.text(jqXHR.responseJSON[k][0]);
                            control.css('display', 'block');

                        }
                    }
                }
            });
        });

        $('body').on('change', '#marca', function () {
            var Id = $(this).val();
            $.ajax({
                url: base_url('getmodelos'),
                type: 'POST',
                data: {id: Id},
                datatype: 'json',
                success: function (r) {
                    r = JSON.parse(r);
                    $('#modelo').empty();
                    $('#modelo')
                        .append($("<option></option>")
                            .attr("selected", "selected")
                            .attr("disabled", "disabled")
                            .text('Elegir..'));
                    $.each(r.result, function (key, value) {
                        $('#modelo')
                            .append($("<option></option>")
                                .attr("value", value.id)
                                .text(value.nombre));
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
        });
        
        $('body').on('change','#region',function(){
            var idRegion=$(this).val();

            $.ajax({
                url: base_url('cargarbases'),
                type: 'POST',
                data: {id:idRegion},
                datatype: 'json',
                success: function (r) {
                    r=JSON.parse(r);

                    $('#base').empty();
                    $('#base')
                        .append($("<option></option>")
                            .attr("selected", "selected")
                            .attr("disabled", "disabled")
                            .text('Elegir..'));
                    $.each(r.result, function (key, value) {
                        $('#base')
                            .append($("<option></option>")
                                .attr("value", value.id)
                                .text(value.nombre));
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) { 
                    
                }
            });
        });
        
    </script>
@endsection