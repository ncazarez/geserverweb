@extends ("layout.basic_plantilla")

@section('content')
    <div class="container">
        <div class="row">
            <div class="pb-2 mt-4 mb-2 border-bottom col-md-12">
                <h2 class="text-center">QR-CODE</h2>
                <div class="float-right">
                    {{ date('d-m-Y H:i:s') }}
                </div>
            </div>
        </div>
        <div class="row">
            {!! QrCode::size(500)->generate( strtoupper( $vehiculo->numero_economico)); !!}
        </div>
        <div class="row">
            <div class="pb-2 mt-4 mb-2 border-top col-md-12 text-center">
                <?php echo DNS1D::getBarcodeSVG("$vehiculo->id", "EAN13",2,40,"black", true)?>
            </div>
        </div>
    </div>
@endsection