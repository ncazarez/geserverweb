@extends ("layout.checklist.plantilla")
@section('content')
    <div class="container">
        @include ("layout.partials.menu_controller")

        <div class="container-fluid">
            <form method="post" action="">
                {{ csrf_field() }}
                <div class="pb-2 mt-8 mb-4 border-bottom">
                    <h5> Ficha del Vehiculo FOLIO: [ {{ $vehiculo->id  }} ]</h5>
                </div>
                <div class="row">
                    <div class="form-group col-md-2">
                        <label>Región</label>
                        <select name="region" id="region" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach ($regiones as $region)
                                @if($region->id==$vehiculo->base->region_id)
                                    <option value="{{$region->id}}" selected>{{$region->nombre}}</option>
                                @else
                                    <option value="{{$region->id}}">{{$region->nombre}}</option>
                                @endif

                            @endforeach
                        </select>
                        <span class="validation-message" data-key="region"></span>
                    </div>

                    <div class="form-group col-md-2">
                        <label>Base</label>
                        <select name="base" class="form-control" id="base">
                            <option selected disabled>Elegir..</option>

                        </select>
                        <span class="validation-message" data-key="base"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Número Económico:</label>
                        <input type="text" name="neconomico" class="form-control" placeholder="Número Económico.."
                               value="{{ strtoupper( $vehiculo->numero_economico) }}">
                        <span class="validation-message" data-key="neconomico"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Número de Serie:</label>
                        <input type="text" name="nserie" class="form-control" placeholder="Número de Serie.."
                               value="{{ strtoupper( $vehiculo->numero_serie)  }}">
                        <span class="validation-message" data-key="nserie"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Marca</label>
                        <select name="marca" id="marca" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach ($marcas as $marca)
                                @if($vehiculo->marca_vehiculo_id==$marca->id)
                                    <option value="{{$marca->id}}" selected>{{$marca->nombre}}</option>
                                @else
                                    <option value="{{$marca->id}}">{{$marca->nombre}}</option>
                                @endif
                            @endforeach
                        </select>
                        <span class="validation-message" data-key="marca"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Modelo</label>
                        <select name="modelo" id="modelo" class="form-control">
                            <option selected disabled>Elegir..</option>
                        </select>
                        <span class="validation-message" data-key="modelo"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Año</label>
                        <select name="anio" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @for ($i = 2020; $i > 1990; $i--)
                                <option value="{{$i}}">{{$i}}</option>
                            @endfor
                        </select>
                        <span class="validation-message" data-key="anio"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Tipo de Vehículo</label>
                        <select name="tipo-vehiculo" class="form-control">
                            <option selected disabled>Elegir..</option>
                            @foreach ($tipos as $tipo)
                                @if($vehiculo->tipo_unidad_id==$tipo->id)
                                    <option value="{{$tipo->id}}" selected>{{$tipo->tipo_unidad}}</option>
                                @else
                                    <option value="{{$tipo->id}}">{{$tipo->tipo_unidad}}</option>
                                @endif

                            @endforeach
                        </select>
                        <span class="validation-message" data-key="tipo-vehiculo"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Capacidad</label>
                        <select name="capacidad" class="form-control">
                            <option selected disabled>Elegir..</option>
                            <option @if($vehiculo->capacidad=='1_3') selected @endif  value="1_3">1 a 3 ton.</option>
                            <option @if($vehiculo->capacidad=='4_7') selected @endif  value="4_7">4 a 7 ton.</option>
                            <option @if($vehiculo->capacidad=='8_12') selected @endif  value="8_12">8 a 12 ton.</option>
                            <option @if($vehiculo->capacidad=='13_20') selected @endif  value="13_20">13 a 20 ton.</option>
                        </select>
                        <span class="validation-message" data-key="capacidad"></span>
                    </div>

                    <div class="form-group col-md-3">
                        <label>¿Requiere Caja?</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input checked class="form-check-input" type="radio" name="requiere-caja"
                                   id="requiere-caja1"
                                   value="1">
                            <label class="form-check-label" for="requiere-caja1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input @if(!$vehiculo->requiere_caja) checked @endif class="form-check-input" type="radio" name="requiere-caja" id="requiere-caja0"
                                   value="0">
                            <label class="form-check-label" for="requiere-caja0">No</label>
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <label>¿Estado activo?</label>
                        <br>
                        <div class="form-check form-check-inline">
                            <input checked class="form-check-input" type="radio" name="estado-activo"
                                   id="estado-activo1"
                                   value="1">
                            <label class="form-check-label" for="estado-activo1">Si</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input @if(!$vehiculo->estado_activo) checked @endif class="form-check-input" type="radio" name="estado-activo" id="estado-activo0"
                                   value="0">
                            <label class="form-check-label" for="estado-activo0">No</label>
                        </div>
                    </div>
                </div>

                <hr>
                <div class="row">
                    <button type="button" class="btn btn-default" style="margin-top: 10px;">
                        <i class="fa fa-oil-can" aria-hidden="true"></i>
                        Cancelar
                    </button>
                    <button type="submit" class="btn btn-primary ml-auto" style="margin-top: 10px;">
                        <i class="fa fa-save" aria-hidden="true"></i> Guardar
                    </button>
                </div>
            </form>

        </div>

        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Security Checklist Manager &copy; V 1.0</span>
                </div>
            </div>
        </footer>
    </div>
@endsection