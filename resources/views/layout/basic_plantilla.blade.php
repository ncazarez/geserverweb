<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <title>{{ env('APP_NAME') }}</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body id="page-top">
<div id="wrapper">
    @yield('content')
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{  asset('vendor/jquery/jquery.min.js')  }}"></script>

<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script src=" {{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<script src=" {{ asset('vendor/moment/min/moment.min.js')}}"></script>

<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

<script src="{{ asset('js/ini.js') }}"></script>


@yield('scripts')

<script>
    function redirect(url) {
        url = url || '';
        window.location.href = base_url(url);
    }

    function base_url(url) {
        url = url || '';
        return '{{ URL::to('/') }}/' + url;
    }
</script>

</body>
</html>