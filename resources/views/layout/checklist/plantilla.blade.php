<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <title>SC Manager - Nuevo Vehículo</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!-- Custom styles for this template-->
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body id="page-top">
<div id="wrapper">
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-clipboard-check"></i>
            </div>
            <div class="sidebar-brand-text mx-3">SC Manager <sup>1</sup></div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item">
            <label class="nav-link">
                <i class="fas fa-clipboard-list"></i>
                <span>Administrador</span></label>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Menu
        </div>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
               aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-user-friends"></i>
                <span>Empleados</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Herramientas:</h6>
                    <a class="collapse-item" href="#">Empleados</a>
                    <a class="collapse-item" href="#">Registrar</a>
                    <a class="collapse-item" href="#">Buscar</a>
                    <a class="collapse-item" href="#">Modificar</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Utilities Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
               aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-truck-moving"></i>
                <span>Vehiculos</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                 data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Herramientas:</h6>
                    <a class="collapse-item" href="{{route('vehiculos')}}">Vehiculos</a>
                    <a class="collapse-item" href="{{ route('nvehiculo') }}">Registro</a>
                    <a class="collapse-item" href="#">Buscar</a>
                    <a class="collapse-item" href="#">Modificar</a>
                </div>
            </div>
        </li>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true"
               aria-controls="collapsePages">
                <i class="fas fa-project-diagram"></i>
                <span>Informes</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages"
                 data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Reportes de seguridad:</h6>
                    <a class="collapse-item" href="#">Reportes</a>
                    <a class="collapse-item" href="#">Reporte operadores</a>
                    <a class="collapse-item" href="#">Reporte Vehiculos</a>
                    <h6 class="collapse-header">Opciones adicionales:</h6>
                    <a class="collapse-item" href="#">Issues</a>
                    <a class="collapse-item" href="#">Puntos de seguridad</a>
                </div>
            </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>

    @yield('content')
</div>

<!-- Bootstrap core JavaScript-->
<script src="{{  asset('vendor/jquery/jquery.min.js')  }}"></script>

<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Core plugin JavaScript-->
<script src=" {{ asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<script src=" {{ asset('vendor/moment/min/moment.min.js')}}"></script>

<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>

<script src="{{ asset('js/ini.js') }}"></script>


@yield('scripts')

<script>
    function redirect(url) {
        url = url || '';
        window.location.href = base_url(url);
    }

    function base_url(url) {
        url = url || '';
        return '{{ URL::to('/') }}/' + url;
    }
</script>

</body>
</html>