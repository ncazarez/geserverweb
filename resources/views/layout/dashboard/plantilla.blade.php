<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="stylesheet" href="/css/app.css">
    @yield('style')
</head>
<body>
<div class="container">
    <nav class="navbar navbar-default" role="navigation">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Desplegar navegación</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img style="max-width:100px; margin-top: -7px;" class="img-responsive" src="{{ asset('img/logos/logo_cemex.png')  }}" alt="">
            </a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">

            <ul class="nav navbar-nav">
                <li>
                    <a href="{{  route('checklist')  }}">CheckList</a>
                </li>
            </ul>


            <ul class="nav navbar-nav navbar-right">
                <li class="navbar-text">Bienvenido: {{ auth()->user()->nombre  }} </li>
                <li class="navbar-text">
                    <form method="post" action="{{ route('logout') }}">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-link">Salir</button>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    @yield('content')
</div>
</body>
</html>