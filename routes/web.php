<?php


/*Route::get('/', function () {
    return view('welcome');
});*/

/*Route::get("/",'Checklist\ChecklistController@index');*/

Route::get("/",'Auth\LoginController@showFormLogin');

Route::get("/dashboard",'Dashboard\DashboardController@index')->name('dashboard');
Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');




Route::get('/checklist' , 'Checklist\ChecklistController@index')->name('checklist');
Route::get("/vehiculos",'Checklist\ChecklistController@vehiculos')->name('vehiculos');
Route::get("/nvehiculo",'Checklist\ChecklistController@nvehiculo')->name('nvehiculo');
Route::get("/qrcode/{id?}","Checklist\ChecklistController@qrcode")->name('qrcode');
Route::get("/vervehiculo/{id?}","Checklist\ChecklistController@vvehiculo")->name('vervehiculo');


Route::post("/getmodelos","Checklist\ChecklistController@ObtenerModelo");
Route::post("/cargarbases","Checklist\ChecklistController@CargarBases");

Route::post("/gvehiculo","Checklist\ChecklistController@guardarvehiculo");









/*
Route::get('qr-code', function () {
    return QrCode::size(400)->generate('SuZuMa');
});
*/


